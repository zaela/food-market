import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {IcBack} from '../../../assets';
import tw from 'twrnc';

const Header = ({title, subTitle, onBack}) => {
  return (
    <View style={tw`${styles.container}`}>
      {onBack && (
        <TouchableOpacity activeOpacity={0.7} onPress={onBack}>
          <View style={tw`${styles.back}`}>
            <IcBack />
          </View>
        </TouchableOpacity>
      )}
      <View>
        <Text style={tw`${styles.title}`}>{title}</Text>
        <Text style={tw`${styles.subTitle}`}>{subTitle}</Text>
      </View>
    </View>
  );
};

export default Header;

const styles = {
  container: 'bg-white px-[1.2rem] pt-[0.9rem] pb-[0.9rem] flex-row',
  title: 'text-[1.2rem] text-[#020202]',
  subTitle: 'text-[0.9rem] text-[#8D92A3]',
  back: 'p-2 mr-4 ml-[-4]',
};
