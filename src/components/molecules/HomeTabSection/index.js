import {useNavigation} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {Dimensions, ScrollView, StyleSheet, Text, useWindowDimensions, View} from 'react-native';
import {SceneMap, TabBar, TabView} from 'react-native-tab-view';
import {useDispatch, useSelector} from 'react-redux';
import { API_HOST } from '../../../config';
import { getFoodDataByTypes } from '../../../redux/action';
import ItemListFood from '../ItemListFood';
// import useInfiniteScroll from "react-infinite-scroll-hook";

const renderTabBar = (props) => (
  <TabBar
    {...props}
    indicatorStyle={styles.indicator}
    style={styles.tabBarStyle}
    tabStyle={styles.tabStyle}
    renderLabel={({route, focused}) => (
      <Text style={styles.tabText(focused)}>{route.title}</Text>
    )}
  />
);

const NewTaste = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {newTaste} = useSelector((state) => state.homeReducer);

  // const [sentryRef] = useInfiniteScroll({
  //   loading,
  //   hasNextPage,
  //   onLoadMore: LoadMore,
  //   // When there is an error, we stop infinite loading.
  //   // It can be reactivated by setting "error" state as undefined.
  //   disabled: !!error,
  //   // `rootMargin` is passed to `IntersectionObserver`.
  //   // We can use it to trigger 'onLoadMore' when the sentry comes near to become
  //   // visible, instead of becoming fully visible on the screen.
  //   rootMargin: '0px 0px 400px 0px',
  // });

  useEffect(() => {
    dispatch(getFoodDataByTypes('new_food'));
  }, []);

  return (
    <ScrollView showsHorizontalScrollIndicator={false}>
      <View style={styles.containerNewTaste}>
        {/* <Text style={{color:'black'}}>hahahha</Text> */}
        {newTaste && (newTaste.map((item) => {
          return (
            // <Text key={item.id}>{item.name}</Text>
            <ItemListFood
              key={item.id}
              type="product"
              name={item.name}
              price={item.price}
              // rating={item.rate}
              rating={Math.round(item.avgRate)}
              image={{uri: API_HOST.storage+item.product_images[0].file_name}}
              onPress={() => navigation.navigate('FoodDetail', item)}
            />
          );
        }))}
      </View>
    </ScrollView>
  );
};

const Popular = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {popular} = useSelector((state) => state.homeReducer);

  useEffect(() => {
    dispatch(getFoodDataByTypes('popular'));
  }, []);

  return (
    <View style={styles.containerPopular}>
      {popular && (popular.map((item) => {
        return (
          <ItemListFood
              key={item.id}
              type="product"
              name={item.name}
              price={item.price}
              rating={Math.round(item.avgRate)}
              image={{uri: API_HOST.storage+item.product_images[0].file_name}}
              onPress={() => navigation.navigate('FoodDetail', item)}
            />
        );
      }))}
    </View>
  );
};

const Recommended = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {recommended} = useSelector((state) => state.homeReducer);

  useEffect(() => {
    dispatch(getFoodDataByTypes('recommended'));
  }, []);

  return (
    <View style={styles.containerRecommended}>
      {recommended && (recommended.map((item) => {
        return (
            item.avgRate ? 
            <ItemListFood
                key={item.id}
                type="product"
                name={item.name}
                price={item.price}
                rating={Math.round(item.avgRate)}
                image={{uri: API_HOST.storage+item.product_images[0].file_name}}
                onPress={() => navigation.navigate('FoodDetail', item)}
            />:''
        );
      }))}
    </View>
  );
};

const initialLayout = {width: Dimensions.get('window').width};
// const initialLayout = {width: useWindowDimensions().width};

const HomeTabSection = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: '1', title: 'New Taste'},
    {key: '2', title: 'Popular'},
    {key: '3', title: 'Recommended'},
  ]);

  const renderScene = SceneMap({
    1: NewTaste,
    2: Popular,
    3: Recommended,
  });

  return (
    <TabView
      renderTabBar={renderTabBar}
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      style={styles.tabView}
    />
  );
};

export default HomeTabSection;

const styles = StyleSheet.create({
  tabView: {backgroundColor: 'white'},
  indicator: {
    backgroundColor: '#020202',
    height: 3,
    width: '15%',
    marginLeft: '3%',
  },
  tabBarStyle: {
    backgroundColor: 'white',
    elevation: 0,
    shadowOpacity: 0,
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1,
  },
  tabStyle: {width: 'auto'},
  tabText: (focused) => ({
    fontFamily: 'Poppins-Medium',
    color: focused ? '#020202' : '#8D92A3',
  }),
  containerNewTaste: {paddingTop: 8, paddingHorizontal: 24},
  containerPopular: {paddingTop: 8, paddingHorizontal: 24},
  containerRecommended: {paddingTop: 8, paddingHorizontal: 24},
});
