export const API_HOST = {
  // url: 'http://127.0.0.1:4000/api',
  url: 'https://ed96-210-210-175-214.ap.ngrok.io/api',
  base_url: 'https://ed96-210-210-175-214.ap.ngrok.io/',
  storage: 'https://ed96-210-210-175-214.ap.ngrok.io/',
};
