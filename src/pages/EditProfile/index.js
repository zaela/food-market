import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import { useDispatch } from 'react-redux';
import {Button, ButtonCustom, Gap, Header, Select, TextInput} from '../../components';
import {API_HOST} from '../../config';
import { getFoodData } from '../../redux/action';
import {getData, showMessage, storeData, useFormCustom} from '../../utils';

const EditProfile = ({navigation}) => {
  const dispatch = useDispatch();
  // const [userId, setUserId] = useState(null)
  const [form, setForm] = useState({
    user_id: '',
    username: '',
    email: '',
    address: '',
    city: '',
    houseNumber: '',
    phoneNumber: '',
  });

  useEffect(() => {
    dispatch(getFoodData());
  }, []);

  useEffect(() => {
    getData('userProfile').then((res) => {
      setForm({
        user_id: res?.id ?? "",
        username: res?.username ?? "",
        email: res?.email ?? "",
        address: res?.address?.detail_address ?? "",
        city: res?.address?.city ?? "",
        houseNumber: res?.address?.house_no ?? "",
        phoneNumber:  res?.address?.phone ? res?.address?.phone.toString() : "",
      });
    });
  }, []);


  const onChange = (name,value) => {
    setForm({
      ...form,
      [name]:value
    })
  }

  const onSubmit = () => {

    getData('token').then((resToken) => {
      Axios.patch(`${API_HOST.url}/user-mobile/edit`, form, {
        headers: {
          Authorization: resToken.value,
        },
      })
        .then((res) => {
          showMessage('Update Success', 'success');
          const profile = res.data.data;
          storeData('userProfile', profile);
          navigation.goBack()
        })
        .catch((err) => {
          showMessage(
            `${err?.response?.data?.message} on Update Profile API` ||
              'Terjadi kesalahan di API Update Profile',
          );
        });
    });
  };

  return (
    <ScrollView contentContainerStyle={styles.scroll}>
      <View style={styles.page}>
        <Header
          title="Edit Profile"
          subTitle="Update your profile"
          onBack={() => navigation.goBack()}
        />
        <View style={styles.container}>
          <TextInput
            label="Full Name"
            placeholder="Type your full name"
            value={form.username}
            onChangeText={(value) => onChange('username', value)}
          />
          <Gap height={14} />
          <TextInput
            label="Email Address"
            placeholder="Type your email address"
            value={form.email}
            onChangeText={(value) => onChange('email', value)}
          />
          <Gap height={14} />
          <TextInput
            label="Address"
            placeholder="Type your address"
            value={form.address}
            onChangeText={(value) => onChange('address', value)}
          />
          <Gap height={14} />
          <TextInput
            label="House Number"
            placeholder="Type your house number"
            type="number"
            value={form.houseNumber}
            onChangeText={(value) => onChange('houseNumber', value)}
            keyboardType="numeric"
          />
          <Gap height={14} />
          <TextInput
            label="Phone Number"
            placeholder="Type your phone number"
            value={form.phoneNumber}
            onChangeText={(value) => onChange('phoneNumber', value)}
            keyboardType="numeric"
          />
          <Gap height={14} />
          <Select
            label="City"
            value={form.city}
            onSelectChange={(value) => onChange('city', value)}
          />
          <Gap height={20} />
          <ButtonCustom text="Update" onPress={onSubmit} />
        </View>
      </View>
    </ScrollView>
  );
};

export default EditProfile;

const styles = StyleSheet.create({
  scroll: {flexGrow: 1},
  page: {flex: 1},
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingVertical: 26,
    marginTop: 24,
    flex: 1,
  },
});
