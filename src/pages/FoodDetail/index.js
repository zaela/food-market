import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {IcBackWhite} from '../../assets';
import {Button, ButtonCustom, Counter, Number, Rating} from '../../components';
import { API_HOST } from '../../config';
import {getData} from '../../utils';

const FoodDetail = ({navigation, route}) => {
  const product = route.params;
  const [totalItem, setTotalItem] = useState(1);
  const [userProfile, setUserProfile] = useState({});

  useEffect(() => {
    getData('userProfile').then((res) => {
      setUserProfile(res);
    });
  }, []);

  const onCounterChange = (value) => {
    setTotalItem(value);
  };


  const onOrder = () => {
    const totalPrice = totalItem * product.price;
    const driver = (totalItem * product.price) * 10 / 100;
    const tax = (10 / 100) * totalPrice;
    const total = totalPrice + driver + tax;

    const data = {
      item: {
        id:product.id,
        name:product.name,
        price:product.price,
        picturePath:product.product_images.length ? product.product_images[0].file_name :'',
      },
      transaction: {
        totalItem,
        totalPrice,
        driver,
        tax,
        total,
      },
      userProfile,
    };

    navigation.navigate('OrderSummary', data);
  };

  // return (
  //   <View>
  //     <Text>Food detail</Text>
  //   </View>
  // )

  return (
    <View style={styles.page}>
      <ImageBackground source={{uri: API_HOST.storage+product.product_images[0].file_name}} style={styles.cover}>
        <TouchableOpacity
          style={styles.back}
          onPress={() => navigation.goBack()}>
          <IcBackWhite />
        </TouchableOpacity>
      </ImageBackground>
      <View style={styles.content}>
        <View style={styles.mainContent}>
          <View style={styles.productContainer}>
            <View>
              <Text style={styles.title}>{product?.name}</Text>
              <Rating number={product?.avgRate ? Math.round(product?.avgRate) : 0} />
            </View>
            <Counter onValueChange={onCounterChange} />
          </View>
          <Text style={styles.desc}>{product?.description}</Text>
          <Text style={styles.label}>Ingredients:</Text>
          <Text style={styles.desc}>Tepung terigu, minyak nabati, tepung tapioka, garam, penstabil, 
          pengatur keasaman, mineral (zat besi), pewarna (tartrazin CI 19140), antioksidan (TBHQ).</Text>
        </View>
        <View style={styles.footer}>
          <View style={styles.priceContainer}>
            <Text style={styles.labelTotal}>Total Price:</Text>
            <Number number={totalItem * product?.price} style={styles.priceTotal} />
          </View>
          <View style={styles.button}>
            <ButtonCustom text="Order Now" onPress={onOrder} />
          </View>
        </View>
      </View> 
    </View>
  );
};

export default FoodDetail;

const styles = StyleSheet.create({
  page: {flex: 1},
  cover: {height: 330, paddingTop: 26, paddingLeft: 22},
  back: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    backgroundColor: 'white',
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    marginTop: -40,
    paddingTop: 26,
    paddingHorizontal: 16,
    flex: 1,
  },
  mainContent: {flex: 1},
  productContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 14,
  },
  title: {fontSize: 16, fontFamily: 'Poppins-Regular', color: '#020202'},
  desc: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: '#8D92A3',
    marginBottom: 16,
  },
  label: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: '#020202',
    marginBottom: 4,
  },
  footer: {flexDirection: 'row', paddingVertical: 16, alignItems: 'center'},
  priceContainer: {flex: 1},
  button: {width: 163},
  labelTotal: {fontSize: 13, fontFamily: 'Poppins-Regular', color: '#8D92A3'},
  priceTotal: {fontSize: 18, fontFamily: 'Poppins-Regular', color: '#020202'},
});
