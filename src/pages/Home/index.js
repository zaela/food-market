import React, {useEffect} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
// import {FoodCard, Gap, HomeProfile, HomeTabSection} from '../../components';
import {FoodCard, Gap, HomeProfile, HomeTabSection} from '../../components';
import { getFoodData } from '../../redux/action';
// import {getFoodData} from '../../redux/action';

const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const {food} = useSelector((state) => state.homeReducer);
//  console.log("food :", food)
  useEffect(() => {
    dispatch(getFoodData());
  }, [navigation]);
  return (
      <View style={styles.page}>
        <HomeProfile />
        <View>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={styles.foodCardContainer}>
              <Gap width={24} />
              {food && (food.map((itemFood) => {
                return (
                  <FoodCard
                    key={itemFood.id}
                    name={itemFood.name}
                    image={itemFood.product_images[0].file_name}
                    // rating={itemFood.rate}
                    rating={Math.round(itemFood.avgRate)}
                    onPress={() => navigation.navigate('FoodDetail', itemFood)}
                  />
                );
              }))}
            </View>
          </ScrollView> 
        </View>
        <View style={styles.tabContainer}>
          <HomeTabSection />
        </View>
      </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  page: {flex: 1},
  foodCardContainer: {flexDirection: 'row', marginVertical: 24},
  tabContainer: {flex: 1},
});
