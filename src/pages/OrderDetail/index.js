import Axios from 'axios';
import React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {ButtonCustom, Gap, Header, ItemListFood, ItemValue} from '../../components';
import {API_HOST} from '../../config';
import {getData, showMessage} from '../../utils';

const OrderDetail = ({route, navigation}) => {
  const order = route.params;

  const onCancel = () => {
    const data = {
      id:order.id,
      status: 'CANCELLED',
    };
    getData('token').then((resToken) => {
      Axios.post(`${API_HOST.url}/transactions/cancel`, data, {
        headers: {
          Authorization: resToken.value,
        },
      })
        .then((res) => {
          navigation.reset({
            index: 0,
            routes: [{name: 'MainApp'}],
          });
        })
        .catch((err) => {
          showMessage(
            `${err?.response?.data?.message} on Cancel Order API` ||
              'Terjadi Kesalahan di Cancel Order API',
          );
        });
    });
  };
  return (
    <ScrollView>
      <Header
        title="Payment"
        subTitle="You deserve better meal"
        onBack={() => navigation.goBack()}
      />
      <View style={styles.content}>
        <Text style={styles.label}>Item Ordered</Text>
        <ItemListFood
          type="order-summary"
          name={order.product.name}
          price={order.product.price}
          items={order.qty}
          image={{uri: API_HOST.storage+order.product.product_images[0].file_name}}
        />
        <Text style={styles.label}>Details Transaction</Text>
        <ItemValue
          label={order.product.name}
          value={order.product.price * order.qty}
          type="currency"
        />
        <ItemValue label="Driver" value={(order.product.price * order.qty) * 10/100} type="currency" />
        <ItemValue
          label="Tax 10%"
          value={(10 / 100) * (order.product.price * order.qty)}
          type="currency"
        />
        <ItemValue
          label="Total Price"
          value={(order.product.price * order.qty) + ((10 / 100) * (order.product.price * order.qty)) + ((10 / 100) * (order.product.price * order.qty))}
          valueColor="#1ABC9C"
          type="currency"
        />
      </View>

      <View style={styles.content}>
        <Text style={styles.label}>Deliver to:</Text>
        <ItemValue label="Name" value={order?.user?.username} />
        <ItemValue label="Phone No." value={order?.user?.address?.phone} />
        <ItemValue label="Address" value={order?.user?.address?.detail_address} />
        <ItemValue label="House No." value={order?.user?.address?.house_no} />
        <ItemValue label="City" value={order?.user?.address?.city} />
      </View>

      <View style={styles.content}>
        <Text style={styles.label}>Order Status:</Text>
        <ItemValue
          label={`#${order.id}`}
          value={order.status}
          valueColor={order.status === 'CANCELLED' ? '#D9435E' : '#1ABC9C'}
        />
      </View>
      <View style={styles.button}>
        {order.status === 'PROCESS' && (
          <ButtonCustom
            text="Cancel My Order"
            onPress={onCancel}
            color="#D9435E"
            textColor="white"
          />
        )}
      </View>
      <Gap height={40} />
    </ScrollView>
  );
};

export default OrderDetail;

const styles = StyleSheet.create({
  content: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingVertical: 16,
    marginTop: 24,
  },
  label: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: '#020202',
    marginBottom: 8,
  },
  button: {paddingHorizontal: 24, marginTop: 24},
});
