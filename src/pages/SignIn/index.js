import React from 'react';
import {StyleSheet, View} from 'react-native';
import {useDispatch} from 'react-redux';
import {ButtonCustom, Gap, Header, TextInput} from '../../components';
import {signInAction} from '../../redux/action/auth';
import {useFormCustom} from '../../utils';
import tw from 'twrnc';
import { API_HOST } from '../../config';
import Axios from 'axios';

const SignIn = ({navigation}) => {
  const [form, setForm] = useFormCustom({
    email: '',
    password: '',
  });
  const dispatch = useDispatch();

  const onSubmit = async () => {
    dispatch(signInAction(form, navigation));
  };

  return (
    <View style={tw`${styles.page}`}>
      <Header title="Sign In" subTitle="Find your best ever meal" />
      <View style={tw`${styles.container}`}>
        <TextInput
          label="Email Address"
          placeholder="Type your email address"
          value={form.email}
          onChangeText={(value) => setForm('email', value)}
        />
        <Gap height={16} />
        <TextInput
          label="Password"
          placeholder="Type your password"
          value={form.password}
          onChangeText={(value) => setForm('password', value)}
          secureTextEntry
        />
        <Gap height={24} />
        <ButtonCustom text="Sign In" onPress={onSubmit} />
        <Gap height={12} />
        <ButtonCustom
          text="Create New Account"
          color="#8D92A3"
          textColor="white"
          onPress={() => navigation.navigate('SignUp')}
        />
      </View>
    </View>
  );
};

export default SignIn;

// const styles = StyleSheet.create({
//   page: {flex: 1},
//   container: {
//     backgroundColor: 'white',
//     paddingHorizontal: 24,
//     paddingVertical: 26,
//     marginTop: 24,
//     flex: 1,
//   },
// });
const styles = {
  page: 'flex-1',
  container: 'bg-white px-[1.2rem] py-[1.3rem] mt-[1rem] flex-1'
};
