import React, {useEffect} from 'react';
import {Text, View, StyleSheet, SafeAreaView, Image} from 'react-native';
import {Logo} from '../../assets';
// import {getData} from '../../utils';
import {Gap} from '../../components';
import tw from 'twrnc';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      // getData('token').then((res) => {
      //   if (res) {
      //     navigation.reset({index: 0, routes: [{name: 'MainApp'}]});
      //   } else {
      //     navigation.replace('SignIn');
      //   }
      // });
      navigation.replace('SignIn');
    }, 2000);
  }, []);

  return (
      <View style={tw`flex-1 items-center justify-center bg-[#FFC700]`}>
        <Logo/>
        <Gap height={38} />
        <Text style={tw`text-[#020202] text-[1.2rem]`}>FoodMarket</Text>
      </View>
  );
};

export default SplashScreen;

