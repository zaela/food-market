import SplashScreen from './SplashScreen';
import SignIn from './SignIn';
import SignUp from './SignUp';
import SignUpAddress from './SignUpAddress';
import SuccessSignup from './SuccessSignUp';
import Home from './Home';
import FoodDetail from './FoodDetail';
import Profile from './Profile';
import Order from './Order';
import OrderDetail from './OrderDetail';
import EditProfile from './EditProfile';
import OrderSummary from './OrderSummary';
import SuccessOrder from './SuccessOrder';

export {
  SplashScreen,
  SignIn,
  SignUp,
  SignUpAddress,
  SuccessSignup,
  Home,
  FoodDetail,
  Profile,
  Order,
  OrderDetail,
  EditProfile,
  OrderSummary,
  SuccessOrder,
};
