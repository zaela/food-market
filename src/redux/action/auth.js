import axios from 'axios';
import {API_HOST} from '../../config';
import {showMessage, storeData} from '../../utils';
import { setLoading } from './global';

export const signUpAction = (dataRegister, address, photoReducer, navigation) => (
  dispatch,
) => {
  axios.post(`${API_HOST.url}/user/register`, dataRegister)
    .then((res) => {
      console.log("register user :", res.data.data);
      const token = `${res.data.data.user.accessToken}`;
      const profile = res.data.data.user;

      storeData('token', {value: token});
      axios.post(`${API_HOST.url}/address/add`, {
        user_id: res.data.data.user.id,
        detail_address: `${address.address}`,
        phone: address.phoneNumber,
        house_no:address.houseNumber,
        city:address.city,

      }).then((res) => {
        storeData('userProfile', profile);
      })


      if (photoReducer.isUploadPhoto) {
        delete photoReducer.isUploadPhoto
        console.log('photoReducer :', photoReducer)
        console.log('res :', res.data.data)

        const photoForUpload = new FormData();
        photoForUpload.append('profile_picture', photoReducer);
        photoForUpload.append('id', res.data.data.user.id);

        axios.patch(`${API_HOST.url}/user-mobile/photo`, photoForUpload, {
          headers: {
            Authorization: token,
            'Content-Type': 'multipart/form-data',
          },
        })
          .then((resUpload) => {
            getData('userProfile').then((resUser) => {
              showMessage('Update Photo Berhasil', 'success');
              resUser.profile_picture = `${resUpload.data.data.profile_picture}`;
              storeData('userProfile', resUser)
            });
          })
          .catch((err) => {
            showMessage(
              err?.response?.message || 'Uplaod photo tidak berhasil',
            );
            navigation.reset({index: 0, routes: [{name: 'SuccessSignUp'}]});
          });
      } else {
        storeData('userProfile', profile);
      }

      navigation.reset({index: 0, routes: [{name: 'SuccessSignUp'}]});
      dispatch(setLoading(false));
    })
    .catch((err) => {
      dispatch(setLoading(false));
      showMessage(err?.response?.data?.message);
    });
};

export const signInAction = (form, navigation) => (dispatch) => {
  dispatch(setLoading(true));
  axios.post(`${API_HOST.url}/user-mobile/login`, form)
  .then((res) => {
      const token = `${res.data.data.accessToken}`;
      const profile = res.data.data.user;
      dispatch(setLoading(false));
      storeData('token', {value: token});
      storeData('userProfile', profile);
      navigation.reset({index: 0, routes: [{name: 'MainApp'}]});
      // navigation.reset({index: 0, routes: [{name: 'SuccessSignUp'}]});
    })
    .catch((err) => {
      dispatch(setLoading(false));
      showMessage(err?.response?.data?.data?.message);
    });
};


