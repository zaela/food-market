import {showMessage} from '../../utils';
import { setLoading } from './global';

const {default: Axios} = require('axios');
const {API_HOST} = require('../../config');

export const getFoodData = () => (dispatch) => {
  dispatch(setLoading(true));
  Axios.get(`${API_HOST.url}/products-mobile/get?limit=10&offset=1`)
    .then((res) => {
      dispatch({type: 'SET_FOOD', value: res.data.data});
      dispatch(setLoading(false));
    })
    .catch((err) => {
      showMessage(
        `${err?.response?.data?.message} on Food API` ||
          'Terjadi kesalahan di API Food',
      );
      dispatch(setLoading(false));
    });
};

export const getFoodDataByTypes = (types) => (dispatch) => {
  dispatch(setLoading(true));
  Axios.get(`${API_HOST.url}/products-mobile/get?limit=10&offset=1&types=${types}`)
    .then((res) => {
      if (types === 'new_food') {
        dispatch({type: 'SET_NEW_TASTE', value: res.data.data});
      }
      if (types === 'popular') {
        dispatch({type: 'SET_POPULAR', value: res.data.data});
      }
      if (types === 'recommended') {
        dispatch({type: 'SET_RECOMMENDED', value: res.data.data});
      }
      dispatch(setLoading(false));
    })
    .catch((err) => {
      showMessage(
        `${err?.response?.data?.message} on Food By Type API` ||
          'Terjadi kesalahan di API Food By Type',
      );
      dispatch(setLoading(false));
    });
};
