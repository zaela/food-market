import {showMessage} from '../../utils';
import { setLoading } from './global';

const {default: Axios} = require('axios');
const {API_HOST} = require('../../config');

export const getUser = (id) => (dispatch) => {
  dispatch(setLoading(true));
  Axios.get(`${API_HOST.url}/user/detail?id=${id}`)
    .then((res) => {
      // dispatch({type: 'SET_FOOD', value: res.data.data});
      dispatch(setLoading(false));
    })
    .catch((err) => {
      showMessage(
        `${err?.response?.data?.message} on User API` ||
          'Terjadi kesalahan di API User',
      );
      dispatch(setLoading(false));
    });
};

export const getAddress = () => (dispatch) => {
  dispatch(setLoading(true));
  Axios.get(`${API_HOST.url}/products-mobile/get?limit=10&offset=1`)
    .then((res) => {
      // dispatch({type: 'SET_FOOD', value: res.data.data});
      dispatch(setLoading(false));
    })
    .catch((err) => {
      showMessage(
        `${err?.response?.data?.message} on User API` ||
          'Terjadi kesalahan di API User',
      );
      dispatch(setLoading(false));
    });
};
