import { io } from "socket.io-client";
import { API_HOST } from "../config";
// const socket = io(API_HOST.base_url);
const socket = io('http://127.0.0.1:4000/');
export default socket;